export const CATEGORIES = {
    'home': 'Home',
    'about': 'About',
    'contacts': 'Contacts',
    'divisions': 'Divisions',
    'admin': 'Admin',
};