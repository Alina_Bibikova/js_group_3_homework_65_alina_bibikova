import React, {Component} from 'react';
import axios from '../../axios-page';
import Spinner from "../../components/UI/Spiner/Spiner";
import {Button, Card, CardBody, CardFooter, CardText, CardTitle} from "reactstrap";
import {NavLink} from "react-router-dom";
import {CATEGORIES} from "../../constans";

class Page extends Component {

    state = {
        category: Object.keys(CATEGORIES)[0],
        page: {},
        loading: true
    };

    loadData() {
        const pageName = this.props.match.params.name;

        axios.get(`/pages/${pageName}.json`).then(response => {
            this.setState({page: response.data});
        }).finally(() => {
            this.setState({loading:  false})
        })
    }

    componentDidMount () {
        this.loadData();
    }

    componentDidUpdate(prevProps){
        if (this.props.match.params !== prevProps.match.params){
            this.loadData();
        }
    }

    render() {

        if (this.state.loading) {
            return <Spinner />
        }

           return (
               <Card>
                   <CardBody>
                       <CardTitle>{this.state.page.title}</CardTitle>
                       <CardText>{this.state.page.content}</CardText>
                   </CardBody>
                   <CardFooter>
                       <NavLink to={'/pages/' + this.props.match.params.name + '/edit'}><Button color="warning">Edit</Button></NavLink>
                   </CardFooter>
               </Card>
           )
    }
}

export default Page;