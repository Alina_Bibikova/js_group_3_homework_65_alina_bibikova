import React, {Component, Fragment} from 'react';
import axios from '../../axios-page';
import PageForm from "../../components/PageForm/PageForm";

class AdminPage extends Component {

    state = {
        page: null
    };

    getPagesUrl = () => {
        const pageName = this.props.match.params.name;
        return 'pages/' + pageName + '.json';
    };

    componentDidMount() {

        axios.get(this.getPagesUrl()).then(response => {
            this.setState({page: response.data});
        })
    }

    editPages = page  => {
        axios.put(this.getPagesUrl(), page).then(() => {
            this.props.history.goBack();
        });
    };

    render() {
        let form = <PageForm
            onSubmit={this.editPages}
            page={this.state.page}
        />;

        if (!this.state.page) {
            form = <div>Loading...</div>
        }
        return (
            <Fragment>
                <h1>Admin page</h1>
                {form}
            </Fragment>
        );
    }
}

export default AdminPage;