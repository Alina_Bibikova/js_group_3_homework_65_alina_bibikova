import React, { Component } from 'react';
import './App.css';
import Layout from "./components/Layout/Layout";
import {Route, Switch} from "react-router";
import Page from "./containers/Page/Page";
import AdminPage from "./containers/AdminPage/AdminPage";
import {Container} from "reactstrap";
import StaticPage from "./components/StaticPage/StaticPage";

class App extends Component {
  render() {
    return (
        <Layout>
            <Container>
            <Switch>
                <Route path="/" exact component={StaticPage} />
                <Route path="/pages/:name/edit" exact component={AdminPage} />
                <Route path="/pages/:name" exact component={Page} />
              <Route render={() => <h1>Hot found</h1>} />
            </Switch>
            </Container>
        </Layout>
    );
  }
}

export default App;
