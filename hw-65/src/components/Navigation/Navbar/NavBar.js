import React from 'react';
import NavigationItems from "../NavigationItems/NavigationItems";
import './NavBar.css';
import NavigationItem from "../NavigationItems/NavigationItem/NavigationItem";

const NavBar = () => {
    return (
        <header className="NavBar">
            <NavigationItem to='/' exact>Static Pages</NavigationItem>
            <nav>
                <NavigationItems />
            </nav>
        </header>
    );
};

export default NavBar;