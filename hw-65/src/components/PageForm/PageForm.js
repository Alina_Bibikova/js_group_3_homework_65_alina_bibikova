import React, {Component} from 'react';
import {CATEGORIES} from "../../constans";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class PageForm extends Component {

    constructor(props) {
        super(props);

        if (props.page) {
            this.state = {...props.page};
        } else {
            this.state = {
                category: Object.keys(CATEGORIES)[0],
                title: '',
                content: '',
            };
        }
    }

    valueChanged = event => {
        const {name, value} = event.target;

        this.setState({[name]: value});
    };

    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <Form className="pageForm" onSubmit={this.submitHandler}>
                <FormGroup row>
                    <Label for="category" sm={2}>Category</Label>
                    <Col sm={10}>
                        <Input type="select" name="category" id="category"
                               onChange={this.valueChanged} value={this.state.category}
                        >
                            {Object.keys(CATEGORIES).map(pageName => (
                                <option key={pageName} value={pageName}>{CATEGORIES[pageName]}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="category" sm={2}>Title</Label>
                    <Col sm={10}>
                        <Input type="text" name="title" id="title"  placeholder="Title"
                               value={this.state.title} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="category" sm={2}>Content</Label>
                    <Col sm={10}>
                        <Input type="textarea" name="content" placeholder="Content"
                               value={this.state.content} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{size: 10, offset: 2}}>
                        <Button color="warning" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default PageForm;