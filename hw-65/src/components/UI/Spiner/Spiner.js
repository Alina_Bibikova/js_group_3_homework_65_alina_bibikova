import React from 'react';
import './Spiner.css';

const Spinner = () => {
    return (
        <div className='Spinner'>
            loading...
        </div>
    );
};

export default Spinner;