import React from 'react';

const StaticPage = () => {
    return (
        <div>
            <h1>Static Pages</h1>
        </div>
    );
};

export default StaticPage;