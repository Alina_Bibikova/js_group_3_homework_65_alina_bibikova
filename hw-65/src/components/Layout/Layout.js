import React, {Fragment} from 'react';
import './Layout.css';
import NavBar from "../Navigation/Navbar/NavBar";

const Layout = ({children}) => (
    <Fragment>
        <NavBar />
        <main className="Layout-Content">
            {children}
        </main>
    </Fragment>
);

export default Layout;